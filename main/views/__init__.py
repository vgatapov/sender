from .mailing import CreateMailing, UpdateMailing, RemoveMailing, MailingStat, MailingStatDetail
from .client import CreateClient, UpdateClient, RemoveClient, ClientDetail
