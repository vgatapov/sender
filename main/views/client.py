from rest_framework.generics import CreateAPIView, UpdateAPIView, DestroyAPIView, RetrieveAPIView
from ..serializers import ClientSerializer
from ..models import Client


class ClientDetail(RetrieveAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class CreateClient(CreateAPIView):
    serializer_class = ClientSerializer


class UpdateClient(UpdateAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class RemoveClient(DestroyAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
