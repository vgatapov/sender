from rest_framework.generics import CreateAPIView, UpdateAPIView, DestroyAPIView, ListAPIView, RetrieveAPIView
from ..serializers import MailingSerializer, MailingStatSerializer, MaillingDetailSerializer
from ..models import Mailing
from django.db.models import Prefetch, Count
from ..models import Message


class CreateMailing(CreateAPIView):
    serializer_class = MailingSerializer


class UpdateMailing(UpdateAPIView):
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer


class RemoveMailing(DestroyAPIView):
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer


class MailingStat(ListAPIView):
    serializer_class = MailingStatSerializer
    queryset = Mailing.objects.all()

    def get_queryset(self):
        return Mailing.objects.prefetch_related(
            Prefetch('message', queryset=Message.objects.all())
        ).annotate(messages_count=Count("message"))


class MailingStatDetail(RetrieveAPIView):
    queryset = Mailing.objects.all()
    serializer_class = MaillingDetailSerializer

    def get_queryset(self):
        return Mailing.objects.prefetch_related(
            Prefetch('message', queryset=Message.objects.all())
        )