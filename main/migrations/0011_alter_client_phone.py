# Generated by Django 3.2.9 on 2022-02-05 12:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0010_auto_20220205_2026'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='phone',
            field=models.BigIntegerField(unique=True),
        ),
    ]
