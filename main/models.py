from django.db import models
from django.conf import settings
from django.utils import timezone
from timezone_field import TimeZoneField
from django.core.validators import MaxValueValidator
from .tasks import send_messages_task
from taggit.managers import TaggableManager
import requests
import logging
import json


logger = logging.getLogger('messages')


class Mailing(models.Model):
    date_start = models.DateTimeField(default=timezone.now)
    date_end = models.DateTimeField(default=timezone.now)
    text = models.CharField(max_length=500, blank=False)
    filters = models.JSONField(null=True)

    def __str__(self):
        return 'Рассылка {}'.format(self.id)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        if self.date_start < timezone.now() < self.date_end:
            send_messages_task.delay(self.id)
        elif self.date_start > timezone.now():
            send_messages_task.apply_async((self.id,), eta=self.date_start)

    def get_clients(self):
        clients = Client.objects.all()
        if self.filters is not None:
            if 'tags' in self.filters:
                clients = clients.filter(tags__name__in=self.filters['tags'])
            if 'code' in self.filters:
                clients = clients.filter(operator_code=self.filters['code'])
        return clients

    def send_messages(self):
        clients = self.get_clients()
        for client in clients:
            message = Message.objects.create(
                mailing=self,
                client=client,
                text='Message to {} with text: {}'.format(client.phone, self.text)
            )
            if self.date_end >= timezone.now():
                message.send()
            else:
                message.status = message.MESSAGE_STATUS_NOT_SENDED
                message.save()

    class Meta:
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'


class Client(models.Model):
    phone = models.BigIntegerField(unique=True)
    operator_code = models.PositiveIntegerField(blank=False, validators=[MaxValueValidator(999)])
    tz = TimeZoneField(default='Europe/London')
    tags = TaggableManager(blank=True)

    def __str__(self):
        return 'Клиент {}'.format(self.phone)

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'


class Message(models.Model):
    MESSAGE_STATUS_CREATED = 0
    MESSAGE_STATUS_SENDED = 1
    MESSAGE_STATUS_FAILED = 2
    MESSAGE_STATUS_NOT_SENDED = 4

    MESSAGE_STATUS_CHOICES = (
        (MESSAGE_STATUS_CREATED, 'Создано'),
        (MESSAGE_STATUS_SENDED, 'Отправлено'),
        (MESSAGE_STATUS_FAILED, 'Ошибка'),
        (MESSAGE_STATUS_NOT_SENDED, 'Не отправлено')
    )

    mailing = models.ForeignKey(Mailing, on_delete=models.CASCADE, related_name='message')
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    create_date = models.DateTimeField(default=timezone.now)
    status = models.IntegerField(choices=MESSAGE_STATUS_CHOICES, default=MESSAGE_STATUS_CREATED)
    text = models.CharField(blank=False, max_length=150, default='')
    log = models.CharField(default='', max_length=1000)

    def __str__(self):
        return 'Сообщение клиенту {}'.format(self.client.id)

    def send(self):
        jwt = settings.PROBE_SERVER_JWT
        request_url = 'https://probe.fbrq.cloud/v1/send/{}'.format(self.id)
        data = {
            'id': self.id,
            'phone': self.client.phone,
            'text': self.text
        }
        headers = {
            "Authorization": "Bearer {}".format(jwt),
            "Content-Type": "application/json",
            "accept": "application/json"
        }
        request = requests.post(request_url, data=json.dumps(data), headers=headers)

        log_text = 'Status: {0} {1}'.format(request.status_code, request.reason)
        self.log = log_text
        if request.status_code == 200:
            self.status = self.MESSAGE_STATUS_SENDED
        else:
            self.status = self.MESSAGE_STATUS_FAILED

        self.save()
        logger.info('Message ID: {0} {1}'.format(self.id, log_text))

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'

