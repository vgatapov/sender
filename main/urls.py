from django.urls import path
from . import views


urlpatterns = [
    path('mailing/create/', views.CreateMailing.as_view()),
    path('mailing/update/<int:pk>/', views.UpdateMailing.as_view()),
    path('mailing/remove/<int:pk>/', views.RemoveMailing.as_view()),
    path('mailing/stat/', views.MailingStat.as_view()),
    path('mailing/stat/<int:pk>/', views.MailingStatDetail.as_view()),

    path('client/<int:pk>/', views.ClientDetail.as_view()),
    path('client/create/', views.CreateClient.as_view()),
    path('client/update/<int:pk>/', views.UpdateClient.as_view()),
    path('client/remove/<int:pk>/', views.RemoveClient.as_view()),
]