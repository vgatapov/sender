from sender.celery import app
from django.apps import apps


@app.task()
def send_messages_task(mailing_id):
    Mailing = apps.get_model('main.Mailing')
    mailing = Mailing.objects.filter(id=mailing_id).first()
    if mailing:
        mailing.send_messages()

