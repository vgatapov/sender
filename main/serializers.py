from rest_framework import serializers
from .models import Mailing, Client, Message
from timezone_field.rest_framework import TimeZoneSerializerField
from taggit.serializers import (TagListSerializerField, TaggitSerializer)
from drf_yasg import openapi


class FiltersJSONField(serializers.JSONField):
    class Meta:
        swagger_schema_fields = {
            "type": openapi.TYPE_OBJECT,
            "required": [],
            "properties": {
                "tags": {
                    "title": "Tags",
                    "description": "List of tags",
                    "type": openapi.TYPE_ARRAY,
                    "items": openapi.TYPE_STRING
                },
                "code": {
                    "title": "Code",
                    "description": "Mobile operator code",
                    "type": openapi.TYPE_INTEGER,
                },
            }
        }


class MailingSerializer(serializers.ModelSerializer):
    filters = FiltersJSONField()

    class Meta:
        model = Mailing
        fields = ['date_start', 'date_end', 'text', 'filters']


class ClientSerializer(TaggitSerializer, serializers.ModelSerializer):
    tags = TagListSerializerField()
    tz = TimeZoneSerializerField()

    def validate_phone(self, value):
        print(value)
        if str(abs(value))[0] != '7':
            raise serializers.ValidationError('Номер должен начинаться с 7')
        if len(str(value)) != 11:
            raise serializers.ValidationError('Цифр должно быть 11')
        return value

    class Meta:
        model = Client
        fields = '__all__'


class MessageSerializer(serializers.ModelSerializer):

    class Meta:
        model = Message
        fields = ['id', 'status', 'create_date', 'text', 'client']


class MailingStatSerializer(serializers.ModelSerializer):
    # message = MessageSerializer(many=True)
    message = serializers.SerializerMethodField()
    messages_count = serializers.IntegerField()

    def get_message(self, obj):
        return {
            'failed': obj.message.filter(status=Message.MESSAGE_STATUS_FAILED).count(),
            'crated': obj.message.filter(status=Message.MESSAGE_STATUS_CREATED).count(),
            'sended': obj.message.filter(status=Message.MESSAGE_STATUS_SENDED).count(),
            'not_sended': obj.message.filter(status=Message.MESSAGE_STATUS_NOT_SENDED).count(),
        }

    class Meta:
        model = Mailing
        fields = ['id', 'text', 'filters', 'message', 'messages_count']


class MaillingDetailSerializer(serializers.ModelSerializer):
    message = MessageSerializer(many=True)

    class Meta:
        model = Mailing
        fields = ['id', 'text', 'filters', 'message']
