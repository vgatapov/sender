import os
from .settings import BASE_DIR

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'full': {
            'format': '{levelname} {asctime} {module} {process:d} {thread:d} {message}',
            'style': '{',
        },
        'simple': {
            'format': '{levelname} {asctime} {message}',
            'style': '{',
        },
    },
    'handlers': {
        'messages': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': os.path.join(BASE_DIR, 'logs/messages.log'),
            'formatter': 'simple'
        }
    },
    'loggers': {
        'messages': {
            'handlers': ['messages'],
            'level': 'INFO',
            'propagate': True,
        }
    },
}