import environ
from .settings import BASE_DIR
import os


env = environ.Env()
environ.Env.read_env(os.path.join(BASE_DIR, ".env"))

REDIS_URL = env("REDIS_URL")

DEBUG = env.bool("DEBUG", default=False)
SECRET_KEY = env("SECRET_KEY")

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': env('DATABASE_NAME'),
        'USER': env('DATABASE_USER'),
        'PASSWORD': env('DATABASE_PASSWORD'),
        'HOST': env('DATABASE_HOST'),
        'PORT': env('DATABASE_PORT'),
    }
}

PROBE_SERVER_JWT = env('PROBE_SERVER_JWT')
