from django.conf import settings


CELERY_TIMEZONE = 'UTC'
CELERY_BROKER_URL = settings.REDIS_URL + '/5'
CELERY_RESULT_BACKEND = settings.REDIS_URL + '/6'
