# Fabrique Message Sender Backend

Backend part for Fabrique Message Sender

## Requirements

Python 3.8+

Redis

PostgreSQL

## Getting started
Create database

```
sudo -u postgres psql
CREATE DATABASE sender;
CREATE USER sender_user WITH password 'sender_password';
GRANT ALL ON DATABASE sender TO sender_user;
\q

```

Init project:
```
git clone https://gitlab.com/vgatapov/sender.git
cd sender
pip3 install -r requirements.txt
```

Create .env file

```
cp .env.sample .env
```
Edit .env file

```
DEBUG= for enable debug 1 or 0 to disable debug
SECRET_KEY=secret key

DATABASE_NAME=db name
DATABASE_USER=db user
DATABASE_PASSWORD=db user password
DATABASE_HOST=localhost
DATABASE_PORT=5432

REDIS_URL=redis://localhost:6379

PROBE_SERVER_JWT=probe server jwt
```

Run migrations and collectstatic
```
./manage.py migrate
./manage.py collectstatic
```

Create superuser
```
./manage.py createsuperuser
```

Run server
```
./manage.py runserver
celery -A sender worker --loglevel=INFO
```

## OpenAPI

Swagger
```
http://localhost:8000/docs/
```
